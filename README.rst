================
tor-formula
================

Simple Salt formula to install and configure Tor on SUSE based distributions.
Forked from https://github.com/upya4ko/salt-tor-formula - thank you.

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/topics/development/conventions/formulas.html>`_.

Available states
================

.. contents::
    :local:

``tor``
-----------------------

Install and configure Tor.

``tor.install``
-------------------------------

Only install Tor packages.

``tor.torsocks``
-------------------------------

Install and configure torsocks.

