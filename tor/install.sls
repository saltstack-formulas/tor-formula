{%- from "tor/map.jinja" import map with context -%}

install_tor:
  pkg.installed:
    - pkgs: {{ map.pkgs }}
  service.running:
    - name: {{ map.service }}
    - restart: True
    - enable: True
    - require:
      - pkg: install_tor
