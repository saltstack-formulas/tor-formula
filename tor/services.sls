{%- from "tor/map.jinja" import map with context -%}
{%- set services = map.hidden_services -%}
{%- set servicedir = map.hs_directory ~ '/' -%}

{%- macro permissions(type='f') -%}
    - user: tor
    - group: tor
    {%- if type == 'f' %}
    - mode: 600
    {%- elif type == 'd' %}
    - mode: 700
    {%- endif %}
{%- endmacro -%}

tor_hs_directory:
  file.directory:
    - name: {{ servicedir }}
    {{ permissions('d') }}
    - require:
      - pkg: install_tor

{%- for service, config in services.items() %}
{%- set myservicedir = servicedir ~ service ~ '/' %}

tor_hs_{{ service }}_directory:
  file.directory:
    - name: {{ myservicedir }}
    {{ permissions('d') }}
    - require:
      - file: tor_hs_directory

{%- for binfile in ['hs_ed25519_public_key', 'hs_ed25519_secret_key'] %}
tor_hs_{{ service }}_{{ binfile }}:
  file.decode:
    - name: {{ myservicedir }}{{ binfile }}
    - contents_pillar: tor:hidden_services:{{ service }}:{{ binfile }}
    - encoding_type: base64
    - require:
      - file: tor_hs_{{ service }}_directory
    - watch_in:
      - service: install_tor

tor_hs_{{ service }}_{{ binfile }}_permissions:
  file.managed:
    - name: {{ myservicedir }}{{ binfile }}
    {{ permissions() }}
    - require:
      - file: tor_hs_{{ service }}_{{ binfile }}
    - watch_in:
      - service: install_tor
{%- endfor %}

tor_hs_{{ service }}_hostname:
  file.managed:
    - name: {{ myservicedir }}hostname
    {{ permissions() }}
    - contents: {{ config['hostname'] }}
    - require:
      - file: tor_hs_{{ service }}_directory
    - watch_in:
      - service: install_tor
{%- endfor %}
