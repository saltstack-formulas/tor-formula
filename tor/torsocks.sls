{%- from "tor/map.jinja" import map with context -%}

install_torsocks:
  pkg.installed:
    - name: torsocks
  service.running:
    - name: {{ map.service_torsocks }}
    - restart: True
    - enable: True
    - require:
      - pkg: install_torsocks

deploy_tor_torsocks:
  file.managed:
    - name: {{ map.config_torsocks }}
    - source: salt://{{ slspath }}/files/ini.jinja
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - defaults:
      config: {{ map.torsocks }}
    - require:
      - pkg: install_torsocks
    - watch_in:
      - service: install_torsocks
